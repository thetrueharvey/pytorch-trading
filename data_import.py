"""
Debugging script
"""
#%% Setup
from cryptotools import *
import talib

from _constants import *

#%% Dataset
while True:
    try:
        for symbol in [s + "USDT" for s in USDT_PAIRS] + [s + "BTC" for s in BTC_PAIRS]:
            for kline in TIMEFRAMES:
                from_binance(symbol=symbol, kline_size=kline, save=True)
    except:
        pass
