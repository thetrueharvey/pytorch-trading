"""
Script for creating a PyTorch dataset
"""
#%% Setup
from tqdm import tqdm
import datatable as dt

from datatable import f, fread
from torch.utils.data import Dataset

from _constants import *

# TODO: Try a Tensor-based approach to slice all pairs simultaneously (i.e. stack a timeframe across all pairs)

#%% Dataset
class CryptoDataset(Dataset):
    def __init__(self, timeframe, horizon_back, horizon_forward, aux_timeframe_mapping):
        self.timeframe = timeframe
        self.horizon_back = horizon_back
        self.horizon_forward = horizon_forward

        self.aux_timeframe_mapping = aux_timeframe_mapping # Determines the number of steps to pull for the various timeframes

        # Get the earliest timestamp common to all pairs
        self.start_time = int(fread(f"data/start_dates.jay")[:, dt.max(f["Start Time"])].to_numpy()[0][0])

        # Determine the amount of available data
        self.data_length = fread(f"data/BTCUSDT-{self.timeframe}-data.jay")[f.Timestamp >= self.start_time,:].shape[0]

        # Determine which timeframes to load
        timeframes = [timeframe] + [timeframe for timeframe in aux_timeframe_mapping]

        # Load the datasets
        self.datasets = {symbol:{} for symbol in [s + "USDT" for s in USDT_PAIRS] + [s + "BTC" for s in BTC_PAIRS]}
        for symbol in [s + "USDT" for s in USDT_PAIRS] + [s + "BTC" for s in BTC_PAIRS]:
            for kline in timeframes:
                self.datasets[symbol][kline] = f"data/{symbol}-{kline}-data.jay"

    def __len__(self):
        return self.data_length - self.horizon_back

    def __getitem__(self, i):
        X = {symbol:{} for symbol in [s + "USDT" for s in USDT_PAIRS] + [s + "BTC" for s in BTC_PAIRS]}
        for symbol in [s + "USDT" for s in USDT_PAIRS] + [s + "BTC" for s in BTC_PAIRS]:
            # Get the primary timeframe
            X[symbol][self.timeframe] = fread(self.datasets[symbol][self.timeframe])[f.Timestamp >= self.start_time,:][i:(i + self.horizon_back), :]

            # Determine the last timestamp
            last_time = X[symbol][self.timeframe][:,dt.min(f["Timestamp"])]

            # Convert to an array
            X[symbol][self.timeframe] = X[symbol][self.timeframe].to_numpy()
            if X[symbol][self.timeframe].shape[0] != self.horizon_back:
                a=0
            #print(X[symbol][self.timeframe].shape)

            for timeframe, horizon_backward in self.aux_timeframe_mapping.items():
                X[symbol][timeframe] = fread(self.datasets[symbol][timeframe])[f.Timestamp <= last_time, :][-(self.aux_timeframe_mapping[timeframe]):, :].to_numpy()
                #print(X[symbol][timeframe].shape)
                if X[symbol][timeframe].shape[0] != self.aux_timeframe_mapping[timeframe]:
                    a = 0

        y = {symbol:{} for symbol in [s + "USDT" for s in USDT_PAIRS] + [s + "BTC" for s in BTC_PAIRS]}
        for symbol in [s + "USDT" for s in USDT_PAIRS] + [s + "BTC" for s in BTC_PAIRS]:
            y[symbol] = fread(self.datasets[symbol][self.timeframe])[(i + self.horizon_back):(i + self.horizon_forward + self.horizon_back), :].to_numpy()
            #print(y[symbol][timeframe].shape)
            if y[symbol].shape[0] != self.horizon_forward:
                    a = 0

        return X,y


